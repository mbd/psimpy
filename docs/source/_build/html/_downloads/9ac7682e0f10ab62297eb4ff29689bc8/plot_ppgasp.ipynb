{
  "cells": [
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "%matplotlib inline"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "\n# PPGaSP: GP emulation for multi-output functions\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "This example shows how to apply Gaussian process emulation to a multi-output\nfunction using class :class:`.PPGaSP`.\n\nThe multi-output function that we are going to look at is the `DIAMOND`\n(diplomatic and military operations in a non-warfighting domain) computer model.\nIt is used as a testbed to illustrate the `PP GaSP` emulator in the `R` package\n`RobustGaSP`, see :cite:t:`Gu2019` for more detail.\n\nThe simulator has $13$ input parameters and $5$ outputs. Namely,\n$\\mathbf{y}=f(\\mathbf{x})$ where $\\mathbf{x}=(x_1,\\ldots,x_{13})^T$\nand $\\mathbf{y}=(y_1,\\ldots,y_5)^T$.\n\nThe training and testing data are provided in the folder '.../tests/data/'.\nWe first load the training and testing data. \n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "import numpy as np\nimport os\n\ndir_data = os.path.abspath('../../../tests/data/')\n\nhumanityX = np.genfromtxt(os.path.join(dir_data, 'humanityX.csv'), delimiter=',')\nhumanityY = np.genfromtxt(os.path.join(dir_data, 'humanityY.csv'), delimiter=',')\nprint(f\"Number of training data points: \", humanityX.shape[0])\nprint(f\"Input dimension: \", humanityX.shape[1])\nprint(f\"Output dimension: \", humanityY.shape[1])\n\nhumanityXt = np.genfromtxt(os.path.join(dir_data, 'humanityXt.csv'), delimiter=',')\nhumanityYt = np.genfromtxt(os.path.join(dir_data, 'humanityYt.csv'), delimiter=',')\nprint(f\"Number of testing data points: \", humanityXt.shape[0])"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "<div class=\"alert alert-info\"><h4>Note</h4><p>You may need to modify ``dir_data`` according to where you save them\n   on your local machine.</p></div>\n\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "``humanityX`` and ``humanitY`` are the training data, corresponding to ``design``\nand ``response`` respectively. ``humanityXt`` are testing input data, at which\nwe are going to make predictions once the emulator is trained. ``humanityYt``\nare the true outputs at ``humanityXt``, which is then used to validate the\nperformance of the trained emulator.\n\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "To build a `PP GaSP` emulator for the above simulator,  first import class\n:class:`.PPGaSP` by\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "from psimpy.emulator import PPGaSP"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Then, create an instance of :class:`.PPGaSP`.  The parameter ``ndim``\n(dimension of function input ``x``) must be specified. Optional parameters, such\nas ``method``, ``kernel_type``, etc., can be set up if desired. Here, we leave\nall the optional parameters to their default values.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "emulator = PPGaSP(ndim=humanityX.shape[1])"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Next, we train the `PP GaSP` emulator based on the training data using\n:py:meth:`.PPGaSP.train` method.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "emulator.train(design=humanityX, response=humanityY)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "With the trained emulator, we can make predictions for any arbitrary set of\ninput points using :py:meth:`PPGaSP.predict` method.\nHere, we make predictions at testing input points ``humanityXt``.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "predictions = emulator.predict(humanityXt)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "We can validate the performance of the trained emulator based on the true outputs\n``humanityYt``.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "import matplotlib.pyplot as plt\n\nfig, ax = plt.subplots(5, 1, figsize=(6,15))\n\nfor i in range(humanityY.shape[1]):\n\n    ax[i].set_xlabel(f'Actual $y_{i+1}$')\n    ax[i].set_ylabel(f'Emulator-predicted $y_{i+1}$')\n    ax[i].set_xlim(np.min(humanityYt[:,i]),np.max(humanityYt[:,i]))\n    ax[i].set_ylim(np.min(humanityYt[:,i]),np.max(humanityYt[:,i]))\n\n    _ = ax[i].plot([np.min(humanityYt[:,i]),np.max(humanityYt[:,i])], [np.min(humanityYt[:,i]),np.max(humanityYt[:,i])])\n    _ = ax[i].errorbar(humanityYt[:,i], predictions[:,i,0], predictions[:,i,3], fmt='.', linestyle='', label='prediction and std')\n    _ = ax[i].legend()\n\nplt.tight_layout()"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "We can also draw any number of samples at testing input ``humanityXt`` using \n:py:meth:`.PPGaSPsample()` method.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "samples = emulator.sample(humanityXt, nsamples=10)\nprint(\"Shape of samples: \", samples.shape)"
      ]
    }
  ],
  "metadata": {
    "kernelspec": {
      "display_name": "Python 3",
      "language": "python",
      "name": "python3"
    },
    "language_info": {
      "codemirror_mode": {
        "name": "ipython",
        "version": 3
      },
      "file_extension": ".py",
      "mimetype": "text/x-python",
      "name": "python",
      "nbconvert_exporter": "python",
      "pygments_lexer": "ipython3",
      "version": "3.9.12"
    }
  },
  "nbformat": 4,
  "nbformat_minor": 0
}
from importlib.metadata import version

__version__ = version("psimpy")
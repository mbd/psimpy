
.. DO NOT EDIT.
.. THIS FILE WAS AUTOMATICALLY GENERATED BY SPHINX-GALLERY.
.. TO MAKE CHANGES, EDIT THE SOURCE PYTHON FILE:
.. "auto_examples/sampler/plot_latin.py"
.. LINE NUMBERS ARE GIVEN BELOW.

.. only:: html

    .. note::
        :class: sphx-glr-download-link-note

        Click :ref:`here <sphx_glr_download_auto_examples_sampler_plot_latin.py>`
        to download the full example code

.. rst-class:: sphx-glr-example-title

.. _sphx_glr_auto_examples_sampler_plot_latin.py:


Latin hypercube sampling
========================

.. GENERATED FROM PYTHON SOURCE LINES 8-11

This example shows how to draw samples using Latin hypercube sampling.



.. GENERATED FROM PYTHON SOURCE LINES 14-17

For the illustration purpose, let's have a look at a two-dimensional example
where we have two random variables X and Y. Each is uniformly distributed in its
range.

.. GENERATED FROM PYTHON SOURCE LINES 18-25

.. code-block:: default


    import numpy as np

    ndim = 2
    # range of X is 10 to 20, range of Y is -10 to 0
    bounds = np.array([[10,20], [-10,0]])








.. GENERATED FROM PYTHON SOURCE LINES 26-28

Given this setting, we can import :class:`.Latin`, create an instance, and
call the :py:meth:`.Latin.sample` method to draw required number of samples

.. GENERATED FROM PYTHON SOURCE LINES 29-36

.. code-block:: default


    from psimpy.sampler import LHS

    # setting seed leads to same samples every time when the codes are run
    lhs_sampler = LHS(ndim, bounds, seed=10)
    lhs_samples = lhs_sampler.sample(nsamples=5)








.. GENERATED FROM PYTHON SOURCE LINES 37-38

The samples are plotted in the following figure.

.. GENERATED FROM PYTHON SOURCE LINES 39-54

.. code-block:: default


    import matplotlib.pyplot as plt

    fig , ax = plt.subplots(figsize=(6,4))

    ax.scatter(lhs_samples[:,0], lhs_samples[:,1], s=10, c='blue', marker='o')

    ax.set_xlabel('X')
    ax.set_ylabel('Y')
    ax.set_xlim(bounds[0])
    ax.set_ylim(bounds[1])
    ax.set_title("Latin hypercube samples (criterion='random')")
    _ = ax.grid(visible=True, which='major', axis='both')





.. image-sg:: /auto_examples/sampler/images/sphx_glr_plot_latin_001.png
   :alt: Latin hypercube samples (criterion='random')
   :srcset: /auto_examples/sampler/images/sphx_glr_plot_latin_001.png
   :class: sphx-glr-single-img





.. GENERATED FROM PYTHON SOURCE LINES 55-58

There are different criterions to pick samples in each hypercube. The default
is `random`, as used above. Other options are `center` and `maximin`. For instance,
we can use the `center` criterion to draw :math:`5` samples as follows:

.. GENERATED FROM PYTHON SOURCE LINES 59-75

.. code-block:: default


    lhs_sampler = LHS(ndim, bounds, criterion='center', seed=10)
    lhs_samples = lhs_sampler.sample(nsamples=5)

    fig , ax = plt.subplots(figsize=(6,4))

    ax.scatter(lhs_samples[:,0], lhs_samples[:,1], s=10, c='blue', marker='o')

    ax.set_xlabel('X')
    ax.set_ylabel('Y')
    ax.set_xlim(bounds[0])
    ax.set_ylim(bounds[1])
    ax.set_title("Latin hypercube samples (criterion='center')")
    _ = ax.grid(visible=True, which='major', axis='both')





.. image-sg:: /auto_examples/sampler/images/sphx_glr_plot_latin_002.png
   :alt: Latin hypercube samples (criterion='center')
   :srcset: /auto_examples/sampler/images/sphx_glr_plot_latin_002.png
   :class: sphx-glr-single-img





.. GENERATED FROM PYTHON SOURCE LINES 76-77

And we can use the `maximin` criterion as follows:

.. GENERATED FROM PYTHON SOURCE LINES 78-91

.. code-block:: default


    lhs_sampler = LHS(ndim, bounds, criterion='maximin', seed=10, iteration=500)
    lhs_samples = lhs_sampler.sample(nsamples=5)

    fig , ax = plt.subplots(figsize=(6,4))

    ax.scatter(lhs_samples[:,0], lhs_samples[:,1], s=10, c='blue', marker='o')

    ax.set_xlabel('X')
    ax.set_ylabel('Y')
    ax.set_xlim(bounds[0])
    ax.set_ylim(bounds[1])
    ax.set_title("Latin hypercube samples (criterion='maximin')")
    _ = ax.grid(visible=True, which='major', axis='both')


.. image-sg:: /auto_examples/sampler/images/sphx_glr_plot_latin_003.png
   :alt: Latin hypercube samples (criterion='maximin')
   :srcset: /auto_examples/sampler/images/sphx_glr_plot_latin_003.png
   :class: sphx-glr-single-img






.. rst-class:: sphx-glr-timing

   **Total running time of the script:** ( 0 minutes  0.270 seconds)


.. _sphx_glr_download_auto_examples_sampler_plot_latin.py:

.. only:: html

  .. container:: sphx-glr-footer sphx-glr-footer-example


    .. container:: sphx-glr-download sphx-glr-download-python

      :download:`Download Python source code: plot_latin.py <plot_latin.py>`

    .. container:: sphx-glr-download sphx-glr-download-jupyter

      :download:`Download Jupyter notebook: plot_latin.ipynb <plot_latin.ipynb>`


.. only:: html

 .. rst-class:: sphx-glr-signature

    `Gallery generated by Sphinx-Gallery <https://sphinx-gallery.github.io>`_

:gitlab_url: https://git.rwth-aachen.de/mbd/psimpy

Welcome to PSimPy's documentation!
==================================

:Release: |release|
:Date: |today|


Contents
--------

.. toctree::
   :maxdepth: 2

   Home <self>
   quickstart
   api
   examples
   changelog
   refs

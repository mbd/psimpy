***********
Sensitivity
***********

This module hosts functionality for sensitivity analysis methods.

Currently implemented classes are:

* :class:`.SobolAnalyze`: Compute Sobol' sensitivity indices.

.. toctree::
   :maxdepth: 1
   :hidden:
   :caption: Sensitivity

    Sobol Analyze <sobol>
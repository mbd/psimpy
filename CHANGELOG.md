v0.1.2
------

* Elaborate on installation. Use pip from conda-forge channel to install `PSimPy`

* Add info about how tests can be run

v0.1.1
------

* Add info about installation

* Change np.float128 to float to avoid issues in windows system

* Update metadata

v0.1.0
------

* First release

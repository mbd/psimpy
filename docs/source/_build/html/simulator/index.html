<!DOCTYPE html>
<html class="writer-html5" lang="en" >
<head>
  <meta charset="utf-8" /><meta name="generator" content="Docutils 0.18.1: http://docutils.sourceforge.net/" />

  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Simulator &mdash; PSimPy&#39;s documentation</title>
      <link rel="stylesheet" href="../_static/pygments.css" type="text/css" />
      <link rel="stylesheet" href="../_static/css/theme.css" type="text/css" />
      <link rel="stylesheet" href="../_static/sg_gallery.css" type="text/css" />
      <link rel="stylesheet" href="../_static/sg_gallery-binder.css" type="text/css" />
      <link rel="stylesheet" href="../_static/sg_gallery-dataframe.css" type="text/css" />
      <link rel="stylesheet" href="../_static/sg_gallery-rendered-html.css" type="text/css" />
      <link rel="stylesheet" href="../_static/css/custom.css" type="text/css" />
  <!--[if lt IE 9]>
    <script src="../_static/js/html5shiv.min.js"></script>
  <![endif]-->
  
        <script src="../_static/jquery.js?v=5d32c60e"></script>
        <script src="../_static/_sphinx_javascript_frameworks_compat.js?v=2cd50e6c"></script>
        <script src="../_static/documentation_options.js?v=37f418d5"></script>
        <script src="../_static/doctools.js?v=888ff710"></script>
        <script src="../_static/sphinx_highlight.js?v=dc90522c"></script>
        <script async="async" src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
    <script src="../_static/js/theme.js"></script>
    <link rel="index" title="Index" href="../genindex.html" />
    <link rel="search" title="Search" href="../search.html" />
    <link rel="next" title="Run Simulator" href="run_simulator.html" />
    <link rel="prev" title="Sobol’ Sensitivity Analysis" href="../sensitivity/sobol.html" /> 
</head>

<body class="wy-body-for-nav"> 
  <div class="wy-grid-for-nav">
    <nav data-toggle="wy-nav-shift" class="wy-nav-side">
      <div class="wy-side-scroll">
        <div class="wy-side-nav-search" >

          
          
          <a href="../index.html" class="icon icon-home">
            psimpy
          </a>
              <div class="version">
                0.2.1
              </div>
<div role="search">
  <form id="rtd-search-form" class="wy-form" action="../search.html" method="get">
    <input type="text" name="q" placeholder="Search docs" aria-label="Search docs" />
    <input type="hidden" name="check_keywords" value="yes" />
    <input type="hidden" name="area" value="default" />
  </form>
</div>
        </div><div class="wy-menu wy-menu-vertical" data-spy="affix" role="navigation" aria-label="Navigation menu">
              <ul class="current">
<li class="toctree-l1"><a class="reference internal" href="../index.html">Home</a></li>
<li class="toctree-l1"><a class="reference internal" href="../quickstart.html">Getting Started</a></li>
<li class="toctree-l1 current"><a class="reference internal" href="../api.html">API &amp; Theory</a><ul class="current">
<li class="toctree-l2"><a class="reference internal" href="../emulator/index.html">Emulator</a></li>
<li class="toctree-l2"><a class="reference internal" href="../inference/index.html">Inference</a></li>
<li class="toctree-l2"><a class="reference internal" href="../sampler/index.html">Sampler</a></li>
<li class="toctree-l2"><a class="reference internal" href="../sensitivity/index.html">Sensitivity</a></li>
<li class="toctree-l2 current"><a class="current reference internal" href="#">Simulator</a><ul>
<li class="toctree-l3"><a class="reference internal" href="run_simulator.html"> Run Simulator</a></li>
<li class="toctree-l3"><a class="reference internal" href="mass_point_model.html"> Mass Point Model</a></li>
<li class="toctree-l3"><a class="reference internal" href="ravaflow3G.html"> Ravaflow Mixture Model</a></li>
</ul>
</li>
</ul>
</li>
<li class="toctree-l1"><a class="reference internal" href="../examples.html">Example Gallery</a></li>
<li class="toctree-l1"><a class="reference internal" href="../changelog.html">Changes</a></li>
<li class="toctree-l1"><a class="reference internal" href="../refs.html">References</a></li>
</ul>

        </div>
      </div>
    </nav>

    <section data-toggle="wy-nav-shift" class="wy-nav-content-wrap"><nav class="wy-nav-top" aria-label="Mobile navigation menu" >
          <i data-toggle="wy-nav-top" class="fa fa-bars"></i>
          <a href="../index.html">psimpy</a>
      </nav>

      <div class="wy-nav-content">
        <div class="rst-content">
          <div role="navigation" aria-label="Page navigation">
  <ul class="wy-breadcrumbs">
      <li><a href="../index.html" class="icon icon-home" aria-label="Home"></a></li>
          <li class="breadcrumb-item"><a href="../api.html">API &amp; Theory</a></li>
      <li class="breadcrumb-item active">Simulator</li>
      <li class="wy-breadcrumbs-aside">
            <a href="../_sources/simulator/index.rst.txt" rel="nofollow"> View page source</a>
      </li>
  </ul>
  <hr/>
</div>
          <div role="main" class="document" itemscope="itemscope" itemtype="http://schema.org/Article">
           <div itemprop="articleBody">
             
  <section id="simulator">
<h1>Simulator<a class="headerlink" href="#simulator" title="Link to this heading"></a></h1>
<p>Computer simulations are widely used to study real-world systems in many fields.
Such a simulation model, so-called <cite>simulator</cite>, essentially defines a mapping from
an input space <span class="math notranslate nohighlight">\(\mathcal{X}\)</span> to an output space <span class="math notranslate nohighlight">\(\mathcal{Y}\)</span>.
More specifically, given a simulator <span class="math notranslate nohighlight">\(\mathbf{y}=\mathbf{f}(\mathbf{x})\)</span>,
it maps a <span class="math notranslate nohighlight">\(p\)</span>-dimensional input <span class="math notranslate nohighlight">\(\mathbf{x} \in \mathcal{X} \subset{\mathbb{R}^p}\)</span>
to a <span class="math notranslate nohighlight">\(k\)</span>-dimensional output <span class="math notranslate nohighlight">\(\mathbf{y} \in \mathcal{Y} \subset{\mathbb{R}^k}\)</span>.
The mapping <span class="math notranslate nohighlight">\(\mathbf{f}\)</span> can vary from simple linear equations which can be analytically
solved to complex partial differential equations which requires numerical schemes such as
finite element methods.</p>
<p>This module hosts simulators and functionality for running simulators. Currently
implemented classes are:</p>
<ul class="simple">
<li><p><a class="reference internal" href="run_simulator.html#psimpy.simulator.run_simulator.RunSimulator" title="psimpy.simulator.run_simulator.RunSimulator"><code class="xref py py-class docutils literal notranslate"><span class="pre">RunSimulator</span></code></a>: Serial and parallel execution of simulators.</p></li>
<li><p><a class="reference internal" href="mass_point_model.html#psimpy.simulator.mass_point_model.MassPointModel" title="psimpy.simulator.mass_point_model.MassPointModel"><code class="xref py py-class docutils literal notranslate"><span class="pre">MassPointModel</span></code></a>: Mass point model for landslide run-out simulation.</p></li>
<li><p><a class="reference internal" href="ravaflow3G.html#psimpy.simulator.ravaflow3G.Ravaflow3GMixture" title="psimpy.simulator.ravaflow3G.Ravaflow3GMixture"><code class="xref py py-class docutils literal notranslate"><span class="pre">Ravaflow3GMixture</span></code></a>: Voellmy-type shallow flow model for landslide run-out simulation.</p></li>
</ul>
<div class="toctree-wrapper compound">
</div>
<div class="admonition note">
<p class="admonition-title">Note</p>
<p><a class="reference internal" href="mass_point_model.html#psimpy.simulator.mass_point_model.MassPointModel" title="psimpy.simulator.mass_point_model.MassPointModel"><code class="xref py py-class docutils literal notranslate"><span class="pre">MassPointModel</span></code></a> and <a class="reference internal" href="ravaflow3G.html#psimpy.simulator.ravaflow3G.Ravaflow3GMixture" title="psimpy.simulator.ravaflow3G.Ravaflow3GMixture"><code class="xref py py-class docutils literal notranslate"><span class="pre">Ravaflow3GMixture</span></code></a> are only relevant if
the user wants to perform run-out simulation. <a class="reference internal" href="mass_point_model.html#psimpy.simulator.mass_point_model.MassPointModel" title="psimpy.simulator.mass_point_model.MassPointModel"><code class="xref py py-class docutils literal notranslate"><span class="pre">MassPointModel</span></code></a> is purely
Python-based and can be used right away. <a class="reference internal" href="ravaflow3G.html#psimpy.simulator.ravaflow3G.Ravaflow3GMixture" title="psimpy.simulator.ravaflow3G.Ravaflow3GMixture"><code class="xref py py-class docutils literal notranslate"><span class="pre">Ravaflow3GMixture</span></code></a> depends on
<a class="reference external" href="https://www.landslidemodels.org/r.avaflow/">r.avaflow 3G</a>, which needs to be
installed by the user.</p>
</div>
</section>


           </div>
          </div>
          <footer><div class="rst-footer-buttons" role="navigation" aria-label="Footer">
        <a href="../sensitivity/sobol.html" class="btn btn-neutral float-left" title="Sobol’ Sensitivity Analysis" accesskey="p" rel="prev"><span class="fa fa-arrow-circle-left" aria-hidden="true"></span> Previous</a>
        <a href="run_simulator.html" class="btn btn-neutral float-right" title="Run Simulator" accesskey="n" rel="next">Next <span class="fa fa-arrow-circle-right" aria-hidden="true"></span></a>
    </div>

  <hr/>

  <div role="contentinfo">
    <p>&#169; Copyright 2024, Hu Zhao.</p>
  </div>

  Built with <a href="https://www.sphinx-doc.org/">Sphinx</a> using a
    <a href="https://github.com/readthedocs/sphinx_rtd_theme">theme</a>
    provided by <a href="https://readthedocs.org">Read the Docs</a>.
   

</footer>
        </div>
      </div>
    </section>
  </div>
  <script>
      jQuery(function () {
          SphinxRtdTheme.Navigation.enable(true);
      });
  </script> 

</body>
</html>

Getting Started
===============

Overview
--------

PSimPy (Predictive and probabilistic simulation with Python) implements
a Gaussian process emulation-based framework that enables systematically and
efficiently performing uncertainty-related analyses of physics-based
models, which are often computationlly expensive. Examples are variance-based
global sensitvity analysis, uncertainty quantification, and parameter
calibration.

Installation
------------

:py:mod:`PSimPy` is a pure Python package. All Python-related dependencies are
automatically taken care of. It should be noted that some modules rely on or use
non-Python package and software. More specifically, the emulator module
:py:mod:`robustgasp.py` relies on the R package
`RobustGaSP <https://cran.r-project.org/web/packages/RobustGaSP/>`_;
the simulator module :py:mod:`ravaflow24.py` relies on the open source software
`r.avaflow 2.4 <https://www.landslidemodels.org/r.avaflow/>`_.

If the simulator module :py:mod:`ravaflow24.py` is desired, you may follow the
official `r.avaflow User manual <https://www.landslidemodels.org/r.avaflow/manual.php>`_
to install it. The installation of the R package `RobustGaSP` is covered in
following steps.

We recommond you to install :py:mod:`PSimPy` in a virtual environment such as a
`conda` environment. You may want to first install `Anaconda` or `Miniconda` if you
haven't. The steps afterwards are as follows.

1. Create a conda environment with Python 3.9::
    
    conda create --name your_env_name python=3.9

2. Install `R` if you don't have it on your machine (if you have `R`, you can
   skip this step; alternatively, you can still follow this step to install `R` in
   the conda environment)::
    
    conda activate your_env_name
    conda install -c conda-forge r-base=3.6

3. Install the R package `RobustGaSP` in the R terminal::
    
    R
    install.packages("RobustGaSP",repos="https://cran.r-project.org",version="0.6.4")

   Try if it is successfully installed by::
    
    library("RobustGaSP")

4. Configure the environment variable `R_HOME` so that `rpy2` knows where to find
   `R` packages. You can find your `R_HOME` by typing the following command in the
   R terminal::
    
    R.home()
    q()

   It is a path like ".../lib/R". Set the environment variable `R_HOME` in your
   conda environment by::
    
    conda env config vars set R_HOME=your_R_HOME

   Afterwards reactivate your conda environment to make the change take effect by::
    
    conda deactivate
    conda activate your_env_name

5. Install :py:mod:`PSimPy` using `pip` in your conda environment by::
    
    conda install -c conda-forge pip
    pip install psimpy

Now you should have :py:mod:`PSimPy` and its dependencies successfully installed
in your conda environment. You can use it in the Python terminal or in your
Python IDE.

If you would like to use it with Jupyter Notebook (iPython Notebook), there is
one extra step needed to set your conda environment on your Notebook:

6. Install `ipykernel` and install a kernel that points to your conda environment
   by::

    conda install -c conda-forge ipykernel
    python -m ipykernel install --user --name=your_env_name

Now you can start your Notebook, change the kernel to your conda environment, and
use :py:mod:`PSimPy`.